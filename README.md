# Data Processing and Machine Learning Pipeline
## Overview

This pipeline automates the process of downloading, preprocessing, and modeling data for two housing datasets. It uses Snakemake to manage workflow dependencies and execution.

## Pipeline Stages

1. **Data Downloading**: Downloads raw data sets from Google Drive.
2. **Data Preprocessing**:
- One-Hot Encoding: Processes raw data using one-hot encoding for categorical variables.
- Label Encoding: Processes raw data using label encoding for categorical variables.
3. **Model Training**:
- Random Forest: Trains a Random Forest model on the preprocessed data (one-hot encoded).
- Gradient Boosting: Trains a Gradient Boosting model on the preprocessed data (label encoded).

## Output

The pipeline generates the following artifacts for each dataset:

- Processed data files (train and test splits) using both one-hot and label encoding methods.
- Trained machine learning models (Random Forest and Gradient Boosting) saved in .pkl format.

## DAG Visualization

Below is the Directed Acyclic Graph (DAG) of the workflow, illustrating the dependencies between the tasks:
![My photo](dag.png)

## Running the Pipeline

To execute the pipeline, ensure you have Snakemake installed and configured, then run the following command from the project root directory:

```bash
snakemake --cores [N]
```
Where [N] is the number of cores you wish to allocate for the workflow execution.