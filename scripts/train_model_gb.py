import pandas as pd
from sklearn.ensemble import GradientBoostingRegressor
import joblib

def train_model(input_file, output_file):
    data = pd.read_csv(input_file)
    X = data.drop('median_house_value', axis=1)
    y = data['median_house_value']
    
    model = GradientBoostingRegressor()
    model.fit(X, y)
    
    # Сохраняем модель
    joblib.dump(model, output_file)

if __name__ == "__main__":
    train_model(snakemake.input.train, snakemake.output.model)