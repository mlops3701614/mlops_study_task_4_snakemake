import requests
import pandas as pd
from io import StringIO


def download_data(url):
    response = requests.get(url)
    if response.status_code == 200:
        data = response.content.decode('utf-8')  # декодирование данных
        df = pd.read_csv(StringIO(data))  # чтение данных в DataFrame
        return df
    else:
        return None
        
if __name__ == "__main__":
    print(snakemake.params.url)
    data = download_data(snakemake.params.url)
    data.to_csv(snakemake.output[0], index=False)
