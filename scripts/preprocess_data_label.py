import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


def preprocess_data(input_file):
    data = pd.read_csv(input_file)
    hdf = data.copy()

    le = LabelEncoder()
    hdf['ocean_proximity'] = le.fit_transform(hdf['ocean_proximity'])
    
    hdf = hdf.dropna(subset = ['median_house_value'])

    # Разделение данных на тренировочные и тестовые
    train_set, test_set = train_test_split(hdf, test_size=0.2, random_state=42)
    return train_set, test_set


if __name__ == "__main__":
    train_set, test_set = preprocess_data(snakemake.input[0])
    train_set.to_csv(snakemake.output.train, index=False)
    test_set.to_csv(snakemake.output.test, index=False)
