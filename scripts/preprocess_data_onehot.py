import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder


def preprocess_data(input_file):
    data = pd.read_csv(input_file)
    hdf = data.copy()
    
    #Processing of categorical features
    h_cat_en_bedr, h_categories_bedr = hdf['ocean_proximity'].factorize()
    
    oh = OneHotEncoder()
    coded_bedr = oh.fit_transform(h_cat_en_bedr.reshape(-1,1))
    
    proxy_bedr = pd.DataFrame(coded_bedr.toarray(), index = hdf.index, columns = ['1','2','3','4','5'])
    
    hdf = pd.concat([hdf, proxy_bedr], axis = 1)
    
    hdf = hdf.drop(['ocean_proximity'], axis = 1)
    
    hdf = hdf.dropna(subset = ['median_house_value'])
    
    
    # Разделение данных на тренировочные и тестовые
    train_set, test_set = train_test_split(hdf, test_size=0.2, random_state=42)
    return train_set, test_set


if __name__ == "__main__":
    train_set, test_set = preprocess_data(snakemake.input[0])
    train_set.to_csv(snakemake.output.train, index=False)
    test_set.to_csv(snakemake.output.test, index=False)
